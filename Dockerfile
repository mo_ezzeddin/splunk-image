FROM ubuntu:20.04 AS vault-copy
WORKDIR /vault
RUN apt-get update && \
    apt-get install wget -y && \
    apt-get install unzip -y
RUN wget -O vault_1.6.1_linux_amd64.zip "https://releases.hashicorp.com/vault/1.6.1/vault_1.6.1_linux_amd64.zip"
RUN unzip vault_1.6.1_linux_amd64.zip

FROM splunk/splunk:latest
WORKDIR /opt/
ENV SPLUNK_HOME="/opt/splunk"
COPY --from=vault-copy /vault/vault .
COPY splunk_ta_aws_503.tgz ./splunk/etc/apps/
RUN sudo chmod +x splunk/etc/apps/splunk_ta_aws_503.tgz
RUN sudo tar -xvf splunk/etc/apps/splunk_ta_aws_503.tgz
ENTRYPOINT ["/sbin/entrypoint.sh"]
